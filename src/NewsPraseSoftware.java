import okhttp3.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * Created by jitu on 11/19/2017.
 */
public class NewsPraseSoftware {


    public static void newsPrase(String Url, String linkAttributeKey, String linkAttrbuteValue, String headingAttrtbuteKey, String headingAttrtbuteValue, String imageAttrtbuteKey, String imageAttrtbuteValue, String descriptionAttrtbuteKey, String descriptionAttrtbuteValue,String type) throws IOException {

        String newsPaperName = Url;
        int startIndex = newsPaperName.indexOf(".");
        int startIndex1 = newsPaperName.indexOf("/");
        int secondIndex = newsPaperName.indexOf(".", startIndex + 1);
       if(startIndex==10||startIndex==11) {
            newsPaperName = newsPaperName.substring(startIndex + 1, secondIndex);
        }else{
            newsPaperName = newsPaperName.substring(startIndex1+2, startIndex);
        }
        System.out.println("newsPaperName: " + newsPaperName);

        Document document = null;
        try {
            document = Jsoup.connect(Url).timeout(100000).userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").get();
            Element bodyElement = document.select("body").get(0);
            // Elements elements = singlePageElements.getElementsByAttributeValueContaining(linksAttributeKey, linksAttributeValue);
            /**
             * getting all links from a url
             */
            for (int i = 0; i < bodyElement.getElementsByAttributeValueContaining(linkAttributeKey, linkAttrbuteValue).size(); i++) {

                String singlepagelink = "";
                try {
                    Element element = bodyElement.getElementsByAttributeValueContaining(linkAttributeKey, linkAttrbuteValue).get(i);
                    singlepagelink = element.select("a").attr("abs:href");
                    System.out.println("link: "+singlepagelink);
                } catch (Exception e) {
                    System.out.println("no link");
                }

                Document singlepage = Jsoup.connect(singlepagelink).timeout(100000).userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").get();

                String heading;
                try {
                    Element headingElement = singlepage.getElementsByAttributeValueContaining(headingAttrtbuteKey, headingAttrtbuteValue).get(0);
                    heading=headingElement.text();
                    if(heading.length()==0) {
                        heading = headingElement.attr("content");
                    }

                    System.out.println("heading: "+heading);
                } catch (Exception e) {
                    System.out.println("no heading");
                }
                try {
                    Element element2 = singlepage.getElementsByAttributeValueContaining(imageAttrtbuteKey, imageAttrtbuteValue).get(0);
                    String elementsrc1 = element2.select("img").attr("abs:src");
                    System.out.println("image: "+elementsrc1);
                } catch (Exception e) {
                    System.out.println("no image");
                }

                try {
                    String description = singlepage.getElementsByAttributeValueContaining(descriptionAttrtbuteKey, descriptionAttrtbuteValue).text();
                    System.out.println("description: "+description);
                } catch (Exception e) {
                    System.out.println("no description");
                }

            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static void sendDataToDatabase(String pagelink, String sourcename, String title, String imagelink, String description, String language, String type) throws IOException {
        System.out.println("pagelink " + pagelink + " sourcename " + sourcename + " title " + title + " imagelink " + imagelink + "description " + description);
        if (!pagelink.equals("") && !pagelink.equals(null) && !sourcename.equals("") && !sourcename.equals(null) && !title.equals("") && !title.equals(null) && !imagelink.equals("") && !imagelink.equals(null)) {
            OkHttpClient client = new OkHttpClient();
            RequestBody body = new FormBody.Builder()
                    .add("pagelink", pagelink)
                    .add("sourcename", sourcename)
                    .add("title", title)
                    .add("imagelink", imagelink)
                    .add("description", description)
                    .add("language", language)
                    .add("type", type)
                    .build();
            Request request = new Request.Builder()
                    .url("http://bluedeserts.com/mynews/z_set_news_all_f.php")
                    .post(body)
                    .build();
            Response response = client.newCall(request).execute();
            System.out.println(response);
            response.body().close();
        }
        NewsUrl.awtControlDemo.showTextAreaDemo(pagelink);
    }










    /*



     for (Element singlePageElement : singlePageElements) {

        try {

            Element link = singlePageElement.getElementsByTag("a").get(0);
            String singlepagelink = link.attr("abs:href");
            // System.out.println("singlepagelink: " + singlepagelink);

            *//**
             * getting document from singlepagelink
             *//*
            Document singlepage = Jsoup.connect(singlepagelink).timeout(100000).userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").get();
            //System.out.println(singlepage);

            *//**
             * getting title of the singlepage
             *//*

            Element element1 = singlepage.getElementsByAttributeValueContaining(headingAttrtbuteKey, headingAttrtbuteValue).get(0);
            // System.out.println(element1.text());

            *//**
             * getting image of the singlepage
             *//*
            Element element2 = singlepage.getElementsByAttributeValueContaining(imageAttrtbuteKey, imageAttrtbuteValue).get(0);
            String elementsrc1 = element2.select("img").attr("abs:src");
            // System.out.println(elementsrc1);

            *//**
             * getting description of the singlepage
             *//*
            Element element3 = singlepage.getElementsByAttributeValueContaining(descriptionAttrtbuteKey, descriptionAttrtbuteValue).get(0);
            // System.out.println(element3.text());
            String description1 = element3.text().toString().trim();
            String description2 = description1.replace("\"", "-");
            String description3 = description2.replace("'", "");
            //System.out.println("description: " + description3);

            *//**
             * sending to database
             *//*
            // sendDataToDatabase(singlepagelink, sourename, title, imgsrcfrompage, description3, language, type);
        } catch (java.lang.Exception e1) {
            //System.out.println("Exception occures" + e1);

        }
    }*/
}
