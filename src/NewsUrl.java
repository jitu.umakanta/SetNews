import okhttp3.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by jitu on 10/11/2017.
 */
public class NewsUrl {

    public static Frame mainFrame;
    public Label headerLabel;
    public Label statusLabel;
    public static Panel controlPanel;
    static int i = 0;
    final static TextArea commentTextArea = new TextArea("This is a AWT tutorial " + "to make GUI application in Java.", 3, 100);
    static NewsUrl awtControlDemo;
    public NewsUrl() {
        prepareGUI();
    }

    public static void main(String args[]) throws IOException {

        awtControlDemo = new NewsUrl();

        ExecutorService executor = Executors.newFixedThreadPool(5);
        Runnable worker1 = new news1();
        executor.execute(worker1);
        Runnable worker2 = new news2();
        executor.execute(worker2);
        Runnable worker3 = new news3();
        executor.execute(worker3);
        Runnable worker4 = new news4();
        executor.execute(worker4);
        Runnable worker5 = new news5();
        executor.execute(worker5);
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
        commentTextArea.setText("Finished Uploaing");
        System.out.println("Finished all threads");

    }


    public static class news1 implements Runnable {

        @Override
        public void run() {

        }
    }

    public static class news2 implements Runnable {

        @Override
        public void run() {

        }
    }

    public static class news3 implements Runnable {

        @Override
        public void run() {

        }
    }

    public static class news4 implements Runnable {

        @Override
        public void run() {


        }

    }

    public static class news5 implements Runnable {

        @Override
        public void run() {
            try {


               /* NewsPraseSoftware.newsPrase("http://www.cricbuzz.com/cricket-news",
                        "cricbuzz",
                        ", cricbuzz.com",
                        "cb-col-33",
                        "a",
                        "first",
                        "abs:href",
                        "english",
                        "h1",
                        "cb-news-img-section",
                        "img",
                        "first",
                        "abs:src",
                        "class",
                        "cb-nws-dtl-itms",
                        "first",
                        "true",
                        "english",
                        "entertainment");*/


               /* NewsPraseSoftware.newsPrase("http://www.amarujala.com/india-news",
                        "cricbuzz",
                        "class",
                        "listing",
                        "title",
                        "सरकार",
                        "class",
                        "ttl",
                        "class",
                        "imgDv auto",
                        "class",
                        "desc"
                       );*/

              /*  NewsPraseSoftware.newsPrase("http://www.hindustantimes.com/india-news/",
                        "cricbuzz",
                        "class",
                        "media-body",
                        "class",
                        "media-img",
                        "itemprop",
                        "headline",
                        "itemprop",
                        "image",
                        "itemprop",
                        "articlebody"
                );*/

              /*  NewsPraseSoftware.newsPrase("http://www.amarujala.com/entertainment/bollywood",
                        "class",
                        "nwsLst",
                        "class",
                        "ttl",
                        "itemprop",
                        "image",
                        "class",
                        "detail"
                );*/

               /* NewsPraseSoftware.newsPrase("https://timesofindia.indiatimes.com/world",
                        "class",
                        "w_tle",
                        "class",
                        "heading1",
                        "class",
                        "highlight",
                        "class",
                        "Normal"
                );*/




                NewsPraseSoftware.newsPrase("http://www.hindustantimes.com/world-news/",
                        "class",
                        "media-img",
                        "itemprop",
                        "headline",
                        "class",
                        "thumbnail",
                        "class",
                        "story-details",
                        "world"
                );







/*

                NewsPraseSoftware.newsPrase("http://zeenews.india.com/world",
                        "class",
                        "col-sm-6",
                        "class",
                        "article-heading",
                        "class",
                        "field-item",
                        "class",
                        "field-item even",
                        "technology"
                );










               NewsPraseSoftware.newsPrase("https://www.techintor.com/",
                        "class",
                        "entry-title",
                        "class",
                        "entry-title",
                        "class",
                        "content",
                        "class",
                        "entry-content",
                        "technology"
                );


                NewsPraseSoftware.newsPrase("http://indianexpress.com/section/world/",
                        "class",
                        "snaps",
                        "itemprop",
                        "headline",
                        "class",
                        "custom-caption",
                        "class",
                        "full-details",
                        "world"
                );









                NewsPraseSoftware.newsPrase("https://www.pinkvilla.com/",
                        "class",
                        "celeb-article",
                        "class",
                        "font-title",
                        "class",
                        "demo-gallery",
                        "class",
                        "article-page-body",
                        "entertainment"
                );


                NewsPraseSoftware.newsPrase("http://www.cricbuzz.com/cricket-news/latest-news",
                        "class",
                        "cb-col-33",
                        "class",
                        "nws-dtl-hdln",
                        "class",
                        "cb-news-img-section",
                        "class",
                        "cb-nws-dtl-itms",
                        "cricket"
                );*/



            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    private void prepareGUI() {
        mainFrame = new Frame("News Upload To Database");
        mainFrame.setSize(750, 400);
        mainFrame.setLayout(new GridLayout(3, 1));
        mainFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent) {
                System.exit(0);
            }
        });
        headerLabel = new Label();
        // headerLabel.setAlignment(Label.CENTER);
        statusLabel = new Label();
        // statusLabel.setAlignment(Label.CENTER);
        statusLabel.setSize(750, 200);

        controlPanel = new Panel();
        controlPanel.setLayout(new FlowLayout());

        mainFrame.add(headerLabel);
        mainFrame.add(controlPanel);
        mainFrame.add(statusLabel);
        mainFrame.setVisible(true);
    }

    public static void showTextAreaDemo(String pagelink) {
        // headerLabel.setText("Control in action: TextArea");

        // Label commentlabel = new Label("Comments: ", Label.RIGHT);

        controlPanel.removeAll();
        commentTextArea.setText("TOTAL:" + i + " " + pagelink);
        // Button showButton = new Button("Show");

      /*  showButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                statusLabel.setText(commentTextArea.getText());
            }
        });*/

        // controlPanel.add(commentlabel);
        controlPanel.add(commentTextArea);
        // controlPanel.add(showButton);

        mainFrame.setVisible(true);
        i++;
    }
}
