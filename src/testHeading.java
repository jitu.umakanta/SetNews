import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;

/**
 * Created by jitu on 11/20/2017.
 */
public class testHeading {
    public static void main(String[] args) throws IOException {
        Document singlepage = Jsoup.connect("http://www.amarujala.com/bihar/lalu-yadav-says-thanks-to-modi-people-scared-from-cow-than-lion").timeout(100000).userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) Gecko/20100101 Firefox/5.0").get();
        //System.out.println(singlepage);

        /**
         * getting title of the singlepage
         */


        Element element1 = singlepage.getElementsByAttributeValueContaining("class", "ttl").get(0);
        System.out.println(element1.text());

        Element element2 = singlepage.getElementsByAttributeValueContaining("class", "imgDv auto").get(0);
        String elementsrc1 = element2.select("img").attr("abs:src");
        System.out.println(elementsrc1);

        Element element3 = singlepage.getElementsByAttributeValueContaining("class", "desc").get(0);
        System.out.println(element3.text());


    }
}
